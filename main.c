/**
 * main.c
 * 
 */
#include <stdio.h>
#include "matrix.h"

int main () {
    Matrix M;
    MT_Init_Matrix(&M,2,2);
    MT_Add_Value(&M, 1, 1, 1);
    MT_Add_Value(&M, 1, 2, 2);
    MT_Add_Value(&M, 2, 1, 3);
    MT_Add_Value(&M, 2, 2, 4);
    MT_Print_Matrix(&M);
    MT_Free_Matrix(&M);
    MT_Print_Matrix(&M);
    //Print_Matrix(&M);
    return 0;
}