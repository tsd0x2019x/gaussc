/**
 * matrix.h
 * 
 */
#ifndef MATRIX_H
#define MATRIX_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#define MATRIX_API extern

typedef struct {
    unsigned short int row;
    unsigned short int col;
    double ** entries;
} Matrix;

MATRIX_API void MT_Gauss (Matrix * M) {
    int non_zero_rows = 0;
    for (unsigned short int i = 0; i < M->row; i++) {
        if (M->entries[i][0] != 0) {
            non_zero_rows += 1;
        }
    }
    if (non_zero_rows > 0) {
        
    }
}

MATRIX_API void MT_Print_Matrix (Matrix * M) {
    if (!M) { // M == NULL
        printf("NULL\n");
    }
    else {
        for (unsigned short int i = 0; i < M->row; i++) {
            for (unsigned short int j = 0; j < M->col; j++) {
                double val = M->entries[i][j];
                if (val < 0)
                    printf(" %.2f", val);
                else
                    printf("  %.2f", val);
            }
            printf("\n");
        }
    }
    
}

/*
 * MT_Add_Value
 * 
 * @params
 *      row : Row index (1..row)
 *      col : Column index (1..column)
 */
MATRIX_API void MT_Add_Value (Matrix * M, unsigned short int row, unsigned short int col, double val) {
    if (!M)
        return;
    M->entries[row-1][col-1] = val;
}

MATRIX_API void MT_Free_Matrix (Matrix * M) {
    if (!M) { // M == NULL
        printf("Free_Matrix failed\n");
        return;
    }
    for (unsigned short int i = 0; i < M->row; i++) {
        free(M->entries[i]);
        M->entries[i] = NULL;
    }
    free(M->entries); M->entries = NULL;
    M->row = M->col = 0;
}

MATRIX_API void MT_Init_Matrix (Matrix * M, unsigned short int row, unsigned short int col) {
    if (row == 0 || col == 0) {
        printf("WARNING: Invalid row (0) or column (0). Matrix cannot be initialized.\n");
        return;
    }
    M->row = row;
    M->col = col;
    M->entries = (double **) malloc(sizeof(double *) * row);
    for (unsigned short int i = 0; i < row; i++) {
        M->entries[i] = (double *) malloc(sizeof(double) * col);
        for (unsigned short int j = 0; j < col; j++) {
            M->entries[i][j] = 0;
        }
    }
}


#ifdef __cplusplus
}
#endif

#endif /* MATRIX_H */
 